#!/bin/bash

function usage () {

    cat <<EOF

    usages: $0 [options] [files]

    Create plot from Alchemist export files.

    files: <path>{:[colno1,...,colnoN]{:[colname1,...colname2]}}
    
    options:
        -t --title  string            set plot title
        -x --xrange integer           set x axis range
        -y --yrange integer           set y axis range
        -d --dir    string            set input/output directory
        -h --help:                    show this help and exit

    Esempio: $0 -y 200 -t Title file1:[1,2]:["label 1"]

EOF
}

######################################################## PARSE OPTIONS

SHORT="t:d:x:y:c:n:h"
LONG="help,xrange:,yrange:,colno:,colname:,title:,dir:"

eval set -- "$(getopt -n "$0" -o $SHORT -l $LONG -- "$@")"

XRANGE=; YRANGE=; TITLE="Plot"; DIR="."               #default

while true ; do
    case "$1" in
        -t|--title  ) TITLE=$2  ; shift 2 ;;
        -x|--xrange ) XRANGE=$2 ; shift 2 ;;
        -y|--yrange ) YRANGE=$2 ; shift 2 ;;
        -d|--dir    ) DIR=$2    ; shift 2 ;;
        -h|--help   ) usage ; exit 0 ;;
        --) shift ; break ;;
        * ) echo "Invalid options" ; exit 1 ;;
    esac
done

################################################ CONSTRUCT PLOT COMMAND

if [ "$#" == 0 ]; then
    echo "Error: No file specified"; exit 1  
fi

PLOT=""
for fileinfo in "$@"; do

    IFS=',' read -r -a FILEOPT <<< "$fileinfo"

    filename="\"$DIR/${FILEOPT[0]}\""
    colsList=`echo "${FILEOPT[1]}" | sed "s|\[\(.*\)\]|\1|"`
    nameList=`echo "${FILEOPT[2]}" | sed "s|\[\(.*\)\]|\1|"`
    IFS=',' read -r -a COLSLIST <<< "$colsList"
    IFS=',' read -r -a NAMELIST <<< "$nameList"

    if [ "${#COLSLIST[@]}" == 0 ]; then PLOT+="$filename,"; fi

    for key in "${!COLSLIST[@]}"
    do
        if [ "${NAMELIST[$key]}" ]; then
             label="title \"${NAMELIST[$key]}\""
        else label=""; fi

        PLOT+="$filename using 1:${COLSLIST[$key]} $label,"
    done
done

##################################################### CREATE & SHOW PLOT

gnuplot -persist <<EOF

    set terminal svg enhanced background rgb 'white' size 1980 800
    set key horizontal top right Left reverse box opaque spacing 3
    set style data linespoints

    set xlabel "Time (s)"
    set ylabel "Error (device count)"
    set yrange [0:$YRANGE]
    set xrange [0:$XRANGE]

    set title  "$TITLE"
    set output "$DIR/$TITLE.svg"

    plot $PLOT 
EOF

# xdg-open "$DIR/$TITLE.svg"           # show plot

