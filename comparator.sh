#!/bin/bash

dirs=( )
plots=( )
for p in "$@"; do
    if [ "${p: -1}" == ")" ]; then
        plots=( "${plots[@]}" "$p" )
    else
        dirs=( "${dirs[@]}" "$p" )
        k=`echo $p | sed 's|[0-9].*$||'`
        if [ "$outdir" != "$k" -a "$outdir" != "" ]; then
            echo "Error! Folders do not share a common prefix."
            exit 1
        fi
        outdir=$k
    fi
done
if [ -e "$outdir" ]; then
    echo -n "Output directory \"$outdir\" exists: overwrite? [abort,yes,keep] "
    read x
    if [ ! -t 0 ]; then
        echo
    fi
    if [ "$x" == "y" ]; then
        rm -rf "$outdir"
        mkdir "$outdir"
    elif [ "$x" != "k" ]; then
        exit 1
    fi
else
    mkdir "$outdir"
fi
len=${#dirs[@]}
percs=( )
vals=( )
nans=( )
opts="0"
for ((i=1;i<len;i++)); do
    opts="$opts,$i"
done

if [ "$3" == "" ]; then
    echo "usage: $0 dir1 dir2... plots..."
    exit 1
fi

function mediate() {
    v=( `echo $1 | tr -c '0-9.' ' '` )
    echo "(${v[0]}+${v[1]})*100/2" | bc
}

function compare() {
    select=""
    if   [ "${nans[$1]}" -le "${nans[$2]}" -a "${vals[$1]}" -le "${vals[$2]}" ]; then
        select="$select"1
    fi
    if   [ "${nans[$1]}" -ge "${nans[$2]}" -a "${vals[$1]}" -ge "${vals[$2]}" ]; then
        select="$select"2
    fi
    if [ "$select" == "1" -o "$select" == "2" ]; then
        echo "$select"
    else
        echo "n"
    fi
}

function likeness() {
    ./plot_builder.py "$@" "${plots[@]}" 3>&1 >/dev/null 2>&3 | grep 'TOT' | tail -n 1 | sed 's|^.*TOT ||;s|%.*$|%|' | tr -d '\n'
}

function store() {
    idx="$1"
    res="$2"
    percs[idx]="$res"
    vals[idx]="echo $res | sed 's|.*/||' | tr -d '.%\n'"
}

for bunch in `ls ${dirs[0]}/* | sed 's|^.*/\([^/]*\)$|\1|;s|_random-[0-9]*\.[0-9]*|_random-*|' | sort | uniq`; do
    echo -ne "$bunch:\t"
    ef=""
    df=""
    en=0
    dn=0
    for file in "${dirs[0]}"/$bunch; do
        f=`basename $file`
        equal="yes"
        for ((i=1;i<len;i++)); do
            if ! diff <(cat "${dirs[0]}/$f" | head -n -3 | tail -n +8) <(cat "${dirs[i]}/$f" | head -n -3 | tail -n +8) > /dev/null; then
                equal="no"
                break
            fi
        done
        if [ "$equal" == "yes" ]; then
            ef="$ef ${dirs[0]}/$f"
            en=$[en+1]
        else
            df="$df $f"
            dn=$[dn+1]
        fi
    done
    echo -ne "equals: $en/$[en+dn] \tnan/err: "
    if [ $en -le 1 ]; then
        for ((i=0;i<len;i++)); do
            store "$i" `likeness "${dirs[i]}"/$bunch`
            echo -n "${percs[i]} "
        done
        echo
    else
        echo `likeness $ef`
    fi
    for f in $df; do
        echo -ne " $f\tnans:"
        for ((i=0;i<len;i++)); do
            nans[i]=`cat "${dirs[i]}/$f" | tr " " "\n" | grep "NaN" | wc -l | tr -cd '0-9'`
            echo -n "${nans[i]} "
        done
        if [ $en -gt 1 ]; then
            echo -ne "\terrs:"
            for ((i=0;i<len;i++)); do
                store "$i" `likeness $ef "${dirs[i]}/$f"`
                echo -n "${percs[i]} "
            done
        fi
        choice=`for ((i=0;i<len;i++)); do
            echo "${nans[i]} ${vals[i]} $i"
        done | sort -n | head -n 1 | cut -d " " -f 3`
        echo -ne " \treplace [$opts,n,y=$choice]? "
        read x
        if [ ! -t 0 ]; then
            echo
        fi
        if [ "$x" != "y" ]; then choice="$x"; fi
        if [ `echo -n $choice | tr -d '0-9' | wc -c` -eq 0 ]; then
            if [ "$choice" -ge 0 -a "$choice" -lt "$len" ]; then
                cp "${dirs[choice]}/$f" "$outdir/$f"
            fi
        fi
    done
done
